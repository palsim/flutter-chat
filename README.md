# flutter_chat

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

# Notes

- [FLUTTER ERROR - D8: Cannot fit requested classes in a single dex file](https://stackoverflow.com/questions/55591958/flutter-firestore-causing-d8-cannot-fit-requested-classes-in-a-single-dex-file)

- [FLUTTER ERROR - No Firebase App '[DEFAULT]' has been created - call Firebase.initializeApp() in Flutter and Firebase](https://stackoverflow.com/questions/63492211/no-firebase-app-default-has-been-created-call-firebase-initializeapp-in)

- [Flutter @IOS MAIN](https://stackoverflow.com/questions/40567540/is-it-possible-to-develop-ios-apps-with-flutter-on-a-linux-virtual-machine)

- [Flutter @IOS](https://medium.com/flutter-community/developing-and-debugging-flutter-apps-for-ios-without-a-mac-8d362a8ec667)

- [Flutter @IOS](https://blog.codemagic.io/how-to-develop-and-distribute-ios-apps-without-mac-with-flutter-codemagic/)

- [Fluter Codemagic](https://www.youtube.com/watch?v=AdZGj8ZVSyo)

- Ctrl + Shift + P > Dart: Capture Logs

- [Sosumi](https://www.youtube.com/watch?v=6WgjQpm9VWE)

- [Sosumi @Archlinux](https://snapcraft.io/install/sosumi/arch)

- [Flutter @Linux - Ubuntu only?](https://medium.com/flutter-community/flutter-for-desktop-create-and-run-a-desktop-application-ebeb1604f1e0)
