import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import '../widgets/chat/new_message.dart';
import '../widgets/chat/messages.dart';

class ChatScreen extends StatefulWidget {
  const ChatScreen({Key? key}) : super(key: key);

  @override
  State<ChatScreen> createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  @override
  void initState() {
    super.initState();

    final fbm = FirebaseMessaging.instance;
    fbm.requestPermission(); //ios only
    //fbm.configure(); //does not exists

    //get device token to send individual notifications
    //fbm.getToken();

    //subscribe to topic
    fbm.subscribeToTopic("chat");

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      print('Got a message whilst in the foreground!');
      print('onMessage >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
      print('Message data: ${message.data}');

      if (message.notification != null) {
        print(
            'notification: ${(message.notification as RemoteNotification).body}');
      }
    });

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      print('onMessageOpenedApp >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
      print('Message data: ${message.data}');

      if (message.notification != null) {
        print('notification: ${message.notification}');
      }
    });

    FirebaseMessaging.onBackgroundMessage((RemoteMessage message) async {
      print('onBackgroundMessage >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
      print('Message data: ${message.data}');

      if (message.notification != null) {
        print('notification: ${message.notification}');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("FlutterChat"),
        backgroundColor: Theme.of(context).primaryColor,
        actions: [
          DropdownButton(
            underline: Container(),
            icon: Icon(Icons.more_vert,
                color: Theme.of(context).primaryIconTheme.color),
            items: [
              DropdownMenuItem(
                child: Container(
                  child: Row(
                    children: const [
                      Icon(Icons.exit_to_app),
                      SizedBox(
                        width: 8,
                      ),
                      Text("Logout"),
                    ],
                  ),
                ),
                value: "logout",
              ),
            ],
            onChanged: (itemIdentifier) {
              if (itemIdentifier == "logout") {
                FirebaseAuth.instance.signOut();
              }
            },
          ),
        ],
      ),
      body: Container(
        child: Column(
          children: const [
            Expanded(
              child: Messages(),
            ),
            NewMessage(),
          ],
        ),
      ),
    );
  }
}
